import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-distance-report',
  templateUrl: 'distance-report.html',
})
export class DistanceReportPage implements OnInit {

  locationAddress: any;
  locationEndAddress: any;
  distanceReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  // devices1243: any[];
  devices: any;
  portstemp: any;
  datetime: number;
  selectedVehicle: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallDistance: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // this.datetimeStart = moment({ hours: 0 }).format();
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();

    this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
    console.log("today time: ", this.datetimeStart)
    this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicallDistance.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallDistance.stopLoading();
          console.log(err);
        });
  }

  getdistancedevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
  }

  distanceReportData = [];
  getDistanceReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let outerthis = this;
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getDistanceReportApi(new Date(outerthis.datetimeStart).toISOString(), new Date(outerthis.datetimeEnd).toISOString(), this.islogin._id, this.Ignitiondevice_id)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.distanceReport = data;
        if (this.distanceReport.length > 0) {
          this.innerFunc(this.distanceReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallDistance.stopLoading();
        console.log(error);
      })
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    var i = 0, howManyTimes = distanceReport.length;
    function f() {

      outerthis.distanceReportData.push({
        'distance': outerthis.distanceReport[i].distance,
        'Device_Name': outerthis.distanceReport[i].device.Device_Name,
        'end_location': {
          'lat': outerthis.distanceReport[i].endLat,
          'long': outerthis.distanceReport[i].endLng
        },
        'start_location': {
          'lat': outerthis.distanceReport[i].startLat,
          'long': outerthis.distanceReport[i].startLng
        }
      });

      outerthis.start_address(outerthis.distanceReportData[i], i);
      outerthis.end_address(outerthis.distanceReportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.distanceReportData[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.distanceReportData[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test" ,res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.distanceReportData[index].addres = str;
        })
    }
  }

  end_address(item, index) {
    let that = this;
    that.distanceReportData[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.distanceReportData[index].EndLocation = "N/A";
    } else if (item.end_location) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
      .then((res) => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.distanceReportData[index].address = str;
      })
    }
  }

}
